import java.io.IOException;
import java.util.Scanner;

public class task4 {
    public static void main(String[] args) throws IOException {
        int a = 0;

        Scanner scanner = new Scanner(System.in);
        String x = scanner.nextLine();
        String operator = scanner.nextLine();
        String y = scanner.nextLine();

        if (x.contains(".") || y.contains(".")) {
            float p = Float.valueOf(x);
            float t = Float.valueOf(y);

            if ("+".equals(operator)) {
                float z = p + t;
                System.out.println(z);
            }
            else if ("-".equals(operator)) {
                float z = p - t;
                System.out.println(z);
            }
            else if ("*".equals(operator)) {
                float z = p * t;
                System.out.println(z);
            }
            else if ("/".equals(operator)) {
                float z = p / t;
                System.out.println(z);
            }
        } else {
            int p = Integer.valueOf(x);
            int t = Integer.valueOf(y);

            if ("+".equals(operator)) {
                int z = p + t;
                System.out.println(z);
            }
            else if ("-".equals(operator)) {
                int z = p - t;
                System.out.println(z);
            }
            else if ("*".equals(operator)) {
                int z = p * t;
                System.out.println(z);
            }
            else if ("/".equals(operator)) {
                int z = p / t;
                System.out.println(z);
            }
        }
    }
}
