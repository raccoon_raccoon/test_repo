public class task2 {
    public static void main(String[] args) {
        int i = 35;
        String strI = Integer.toString(i);
        System.out.println(strI);
            String b = "123";
            int a = Integer.valueOf(b).intValue();
            System.out.println(a); //преобразование int в строку и обратно

        long l = 1231231;
        String strL = Long.toString(l);
        System.out.println(strL);
            String s = "1231231";
            long k = Long.valueOf(s).longValue();
            System.out.println(k); //преобразование long в строку и обратно

        double  d = 32.4e10;
        String strD = Double.toString(d);
        System.out.println(strD);
            String db = "32.4e10";
            double f = Double.valueOf(db).doubleValue();
            System.out.println(f); //преобразование double в строку и обратно

        float  fl = 3.46f;
        String strF = Float.toString(fl);
        System.out.println(strF);
            String u = "32.4e10";
            float p = Float.valueOf(u).floatValue();
            System.out.println(p); //преобразование float в строку и обратно

        byte bt = 10;
        String strByte = Byte.toString(bt);
        System.out.println(strByte);
            String toByte = "123";
            byte bte = Byte.valueOf(toByte);
            System.out.println(bte); //преобразование byte в строку и обратно

        short shrt = 25678;
        String strShrt = Short.toString(shrt);
        System.out.println(strShrt);
            String sss = "25999";
            short fromString = Short.valueOf(sss);
            System.out.println(fromString);

        boolean bln = true;
        String strBln = Boolean.toString(bln);
        System.out.println(strBln);
            String fBln = "false";
            boolean BfromString = Boolean.valueOf(fBln);
            System.out.println(BfromString);

        char ch = 'Q';
        String oneString = String.valueOf(ch);
        System.out.println(oneString);
            String m = "h";
            char fromLstring = Character.valueOf('m');
            System.out.println(fromLstring);

    }
}
