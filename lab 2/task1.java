class task1 {
    public static void main(String[] args) {
        int a = 3;
        int b = 4;
        int c = a + b;
        System.out.println("c = " + c); //использование арифметического оператора

        int x = 1;
        int y = 2;
        boolean z = x > y;
        System.out.println(z); //использование сравнения

        int height;
        int weight;
        height = 2;
        weight = 60;
        if (height >= 1 && weight >= 60) {
            System.out.println ("You can't go there"); //использование логического оператора

            int d = 1;
            int e = 2;
            boolean f = d == e;
            System.out.println(f); //приравнивание

        }
    }
}

